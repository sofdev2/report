/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.albumproject.service;

import com.yada.albumproject.dao.SaleDao;
import com.yada.albumproject.model.ReportSale;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class ReportService {
    public List<ReportSale> getReportSaleByDay() {
        SaleDao dao = new SaleDao();
        return dao.getDayReport();
    }
    public List<ReportSale> getReportSaleByMonth(int year) {
        SaleDao dao = new SaleDao();
        return dao.getMonthReport(year);
    }
}
